Rails.application.routes.draw do
  resources :links, only: [:index, :show, :create, :info]
  resources :visitors, only: [:index]
  get "/", to: "links#index"
  get "/:token", to: "visitors#index"
  get "/:token/info", to: "links#info"
end
