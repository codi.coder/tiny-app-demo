class CreateVisitors < ActiveRecord::Migration[6.0]
  def change
    create_table :visitors do |t|
      t.string :ip_address
      t.string :token
      t.belongs_to :link, null: false, foreign_key: true

      t.timestamps
    end
  end
end
