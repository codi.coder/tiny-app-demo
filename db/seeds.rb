# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


links = Link.create([
  {
    url: "https://www.google.com",
    token: "goo"
  },
  {
    url: "https://www.chitchats.com",
    token: "ccs"
  }
]);

visitors = Visitor.create([
  {
    ip_address: "192.168.1.8",
    token: "goo",
    link_id: 1
  }, 
  {
    ip_address: "192.168.1.22",
    token: "goo",
    link_id: 1
  },
  {
    ip_address: "192.168.1.23",
    token: "ccs",
    link_id: 2
  },
  {
    ip_address: "192.168.1.8",
    token: "ccs",
    link_id: 2
  }
]);