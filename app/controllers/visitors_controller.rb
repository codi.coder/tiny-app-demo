class VisitorsController < ApplicationController
  def index
    @link = Link.find_by(token: params[:token])
    if @link.present? 
      @visitor = Visitor.new(token: params[:token], ip_address: request.ip(), link_id: @link.id)
      @visitor.save
      redirect_to @link.url
    else
      redirect_to controller: 'links', invalid_token: params[:token], action: 'index'
    end
  end
  
end