class LinksController < ApplicationController
  def index
    @link = Link.new
    @token = params[:token]
    if @token.nil?
      @invalid_token = params[:invalid_token] 
    end
  end

  def create
    link = Link.new(link_params)
    respond_to do |format|
      if link.save
        format.html { redirect_to controller: 'links', token: link.token, action: 'index' }
      end
    end
  end

  def info
    @link = Link.find_by(token: params[:token])
  end

  private
    def link_params
      params.require(:link).permit(:url)
    end
end