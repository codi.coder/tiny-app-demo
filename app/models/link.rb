class Link < ApplicationRecord
  has_many :visitors

  validates_presence_of :url
  validates :url, format: URI::regexp(%w[http https]) #simple check
  validates_uniqueness_of :token
  #character limitation
  validates_length_of :url, within: 3..255, on: :create, message: "please enter between 3 and 255"
  validates_length_of :token, within: 3..255, on: :create, message: "please enter between 3 and 255"

  # reserve for custom token validation
  before_validation :generate_token
  def generate_token
    self.token = SecureRandom.uuid[0..17] if self.token.nil? || self.token.empty?
    true
  end

  def total_clicks
    visitors.all.size
  end

end
